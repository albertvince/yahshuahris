import React, { Component } from 'react';
import { View, Text, Button,AppRegistry} from 'react-native';
import { Header } from 'react-native-elements';

export default class Profile extends Component {
    render() {
        return(
            <View>
            <Header
                leftComponent={{ icon: 'menu', color: '#fff' }}
                onPress={() => this.props.navigation.navigate('DrawerOpen')}
                centerComponent={{ text: 'Profile', style: { color: '#fff' } }}
                // rightComponent={{ icon: 'home', color: '#fff' }}
                />
        </View>    
        );
    }
}

AppRegistry.registerComponent('Profile', () => Profile);