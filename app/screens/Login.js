import React, { Component } from 'react';
import { View, Text,AppRegistry,TextInput, StyleSheet,Image, YellowBox} from 'react-native';
import { Button } from 'react-native-elements';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated']); //temporarily hide the yellow box warning.
YellowBox.ignoreWarnings(['VirtualizedList: missing keys for items']);

export default class Login extends Component {
	constructor() {
		super();
		this.state = {
			username: "",
			password: "",
			error: ""
		}
	}
	
// 	componentDidMount() {
// 		if(this.props.credentials.token) {
// 			this.props.navigation.navigate('App');
// 		}
// 	}

// 	verifyCredentials() {
// 		Keyboard.dismiss();
// 		const { username, password } = this.state;
// 		this.props.dispatch(action.fetchAuthToken({username: username, password: password}))
// 		.then((response) => {	
// 			if(response.token) { 
// 				this.props.navigation.navigate('App');
// 				this.state.animating = false;
// 			}else if(username == '' || password == ''){
// 				ToastAndroid.showWithGravityAndOffset('Please complete the field.',ToastAndroid.SHORT,ToastAndroid.BOTTOM, 0, 75);	
// 			}else {
// 				ToastAndroid.showWithGravityAndOffset('Invalid employee code/password.',ToastAndroid.SHORT,ToastAndroid.BOTTOM, 0, 75);
// 			 }
// 		})
// 	}	

	render() {
		return(
			<View style={styles.container}>
						<Image
							style={styles.logo}
							source={require('../img/job-icon.png')} 
							/>
						<Text style={styles.title}> Yahshua HRIS </Text>   
					
				<View style={styles.inputBox}>
						<Image source={require('../img/email.png')} style={styles.ImageStyle} />
						<TextInput 
						style={{flex:1}}
						autoCapitalize="characters"
						placeholder="Email"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="next"
						onChangeText={(username) => this.setState({username: username})}/>
				</View>

				<View style={styles.inputBox}>
					<Image source={require('../img/passwords.png')} style={styles.ImageStyle} />
					<TextInput
						secureTextEntry={true}
						style={{flex:1}}
						autoCapitalize="characters"
						placeholder="Password"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="go"
						onChangeText={(password) => this.setState({password: password})}
						 />

				</View>
				<View>
					<Button buttonStyle={styles.buttonLogIn} title="Login" textStyle={{ color: "#FFF", fontSize: 20 }}
                    />
                    <Text style={styles.textOr}>----------------or----------------- </Text>
					<Text style={styles.textSignup}>  Sign up </Text>
				</View>
			</View>
		)
	}
}

// function mapStateToProps(state) {
// 	return {
// 		credentials: state.setCredentials,
// 		error: state.errorCredentials,
// 		loading: state.loadingCredentials
// 	}
// }

AppRegistry.registerComponent('Login', () => Login);


const styles = StyleSheet.create({
	container : {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#1D8348'
	},

	buttonLogIn: {
		width: 300,
		paddingVertical :12,
		marginVertical: 10,
		backgroundColor: '#2E86C1',
		borderRadius: 25
	},

	inputBox: {
		width:300,
		backgroundColor: 'rgba(255,255,255,0.3)',
		borderRadius: 25,
		paddingHorizontal:16,
		marginVertical: 7,	
		flexDirection: 'row',
		height: 45,
        margin: 15,
	},

	title:{
		color: 'white',
		marginTop:7,
		fontWeight: 'bold',
		textAlign:'center',
		marginVertical: 50,
		fontSize: 20
    },

    textSignup:{
		color: '#FFF',
		marginTop:7,
		textAlign:'center',
		marginVertical: 50,
        fontSize: 17,
        textDecorationLine:'underline',
    },

    textOr:{
		color: '#FFF',
		marginTop:7,
		textAlign:'center',
        fontSize: 18,
	},

	logo: {
		width:170,
		height:130,
	},

	ImageStyle: {
		padding: 10,
		marginVertical:15,
		height: 12,
		width: 12,
		resizeMode : 'stretch',
		alignItems: 'center'
	}

});

// export default connect(mapStateToProps)(Login)