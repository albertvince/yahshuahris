import React, { Component } from 'react';
import { View, Text, AppRegistry} from 'react-native';
import { Header } from 'react-native-elements';
export default class Home extends Component {
    render() {
        return(
            <View>
                <Header
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    onPress={() => this.props.navigation.navigate('DrawerOpen')}
                    centerComponent={{ text: 'Home', style: { color: '#fff'} }}
                    // rightComponent={{ icon: 'home', color: '#fff' }}
                    />
            </View>    
        );
    }
}
AppRegistry.registerComponent('Home', () => Home);